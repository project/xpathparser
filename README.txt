============================================================================
CONTENTS OF THIS FILE
============================================================================
* Installation
* Configuration and features

============================================================================
INSTALLATION
============================================================================
Quite simple, Download the module and simply put into your 
your_drupal_path/sites/all/modules and install from your admin panel.

You are almost done. Please download latest release of codemirror from 
http://codemirror.net. After downloading codemirror extract into your drupal 
installation folder in following path sites/all/libraries/ and rename it as 
codemirror. If your library path looks like sites/all/libraries/codemirror 
thats mean you are ready.

Now you will have nice editor.


============================================================================
CONFIGURATION
============================================================================
After successful installation choose permission for this module. Please 
browse /admin/people/permissions and set "Full HTML" for suitable user.

Similarly in /admin/people/permissions page find out "Javascript Editor" and 
set "Administer Jstool" for suitable user.

If everything is ready then go to following path 
Admin >> Configuration >> Jstool

Make sure you need write permission in your js file to edit.

Thats it.


Before changing anything be sure what you want to do else it will may break
your site. It will better if you make a copy of js file before saving your
content.

============================================================================
DEPENDENCY
============================================================================
1. Libraries API (http://drupal.org/project/libraries/)
